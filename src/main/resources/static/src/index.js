import * as StompJs from '@stomp/stompjs';

var client;

window.connect = () => {
	var url = "ws://localhost:8081/websocket-endpoint/websocket";
	client = new StompJs.Client({
		brokerURL: url,
		debug: function (str) {
			console.log(str);
		},
		reconnectDelay: 5000,
		heartbeatIncoming: 4000,
		heartbeatOutgoing: 4000
	});

}

window.send = () => {
	// Send message
}

document.body.innerHTML = "<div><button onClick='connect()'>Connect</button>"+
    "<button onClick='send()'>Send</button></div>";
